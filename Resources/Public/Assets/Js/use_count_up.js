var teufels_thm_teufels_cpt_cnt_numbers_interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {

    }  else {

        if (typeof teufels_cfg_typoscript__windowLoad == 'boolean' && teufels_cfg_typoscript__windowLoad) {
            clearInterval(teufels_thm_teufels_cpt_cnt_numbers_interval);

            // FKT isOnScreen
            $.fn.isOnScreen = function(){

                var win = $(window);

                var viewport = {
                    top : win.scrollTop(),
                    left : win.scrollLeft()
                };
                viewport.right = viewport.left + win.width();
                viewport.bottom = viewport.top + win.height();

                var bounds = this.offset();
                console.log(bounds);

                bounds.right = bounds.left + this.outerWidth();
                bounds.bottom = bounds.top + this.outerHeight();

                return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

            };



            var oCountUpNumbers = $('.count-up--value');
            if (oCountUpNumbers.length > 0) {
                var i = 1;
                oCountUpNumbers.each(function (e) {

                    var sSeperator = $(this).attr('data-seperator');
                    var sDecimalSeperator = $(this).attr('data-decimal-seperator');

                    var iTargetElementId = $(this).attr('id');
                    var iDataStart = $(this).attr('data-start');
                    var iDataTarget = $(this).attr('data-target');
                    var iDataDecimals = $(this).attr('data-decimals');
                    var iDataDuration = $(this).attr('data-duration');


                    var options = {
                        useEasing : true,
                        useGrouping : true,
                        separator : sSeperator,
                        decimal : sDecimalSeperator,
                        prefix : '',
                        suffix : ''
                    };

                    var numberCount = new CountUp(iTargetElementId, iDataStart, iDataTarget, iDataDecimals, iDataDuration, options);


                    if ($('#'+iTargetElementId).isOnScreen()) {
                        numberCount.start();
                    } else {
                        $(window).scroll(function(){
                            if ($('#'+iTargetElementId).isOnScreen()) {
                                numberCount.start();
                            }
                        });
                    }

                    // console.log('id:'+iTargetElementId+' start:'+iDataStart+' target:'+iDataTarget+' decimals:'+iDataDecimals+' iDataDuration')
                    i++;
                });
            }
        }



    }

}, 2000);



