<?php

namespace TEUFELS\TeufelsCptCntNumbers\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \TEUFELS\TeufelsCptCntNumbers\Domain\Model\Number.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Dominik Hilser <d.hilser@teufels.com>
 */
class NumberTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \TEUFELS\TeufelsCptCntNumbers\Domain\Model\Number
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \TEUFELS\TeufelsCptCntNumbers\Domain\Model\Number();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getIconReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getIcon()
		);
	}

	/**
	 * @test
	 */
	public function setIconForFileReferenceSetsIcon()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setIcon($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'icon',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getStartReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getStart()
		);
	}

	/**
	 * @test
	 */
	public function setStartForStringSetsStart()
	{
		$this->subject->setStart('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'start',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getNumberReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getNumber()
		);
	}

	/**
	 * @test
	 */
	public function setNumberForStringSetsNumber()
	{
		$this->subject->setNumber('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'number',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getUnitReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getUnit()
		);
	}

	/**
	 * @test
	 */
	public function setUnitForStringSetsUnit()
	{
		$this->subject->setUnit('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'unit',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTeaserReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTeaser()
		);
	}

	/**
	 * @test
	 */
	public function setTeaserForStringSetsTeaser()
	{
		$this->subject->setTeaser('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'teaser',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDurationReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDuration()
		);
	}

	/**
	 * @test
	 */
	public function setDurationForStringSetsDuration()
	{
		$this->subject->setDuration('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'duration',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDecimalsReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDecimals()
		);
	}

	/**
	 * @test
	 */
	public function setDecimalsForStringSetsDecimals()
	{
		$this->subject->setDecimals('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'decimals',
			$this->subject
		);
	}
}
