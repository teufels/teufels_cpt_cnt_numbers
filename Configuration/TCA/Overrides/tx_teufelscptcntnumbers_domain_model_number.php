<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['tx_teufelscptcntnumbers_domain_model_number']['types']['1']['showitem'] = '--div--;General,icon, start, number, unit, title, teaser;;;richtext:rte_transform[mode=ts_links],
--div--;Animation,seperator, duration, decimals, 
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden;;1, starttime, endtime';


$GLOBALS['TCA']['tx_teufelscptcntnumbers_domain_model_number']['ctrl']['label'] = 'title';
//$GLOBALS['TCA']['tx_teufelscptcntnumbers_domain_model_number']['ctrl']['label_alt'] = 'number';
//$GLOBALS['TCA']['tx_teufelscptcntnumbers_domain_model_number']['ctrl']['label_alt_force'] =  1;